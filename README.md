# vueadmin后台管理系统
## 如果对你有帮助，请帮忙点个赞
## 官网
http://www.vueadmin.net

## 特别说明
本系统是完全基于vueadmin系统生成的应用，不具备一键生成功能

## vueadmin介绍
vueadmin是基于vue+element2开发的快速生成系统 无需安装vue环境 无需编译打包  不写一行代码可以快速生成模块 极大的提升工作效率  支持独立的后台生成、cms生成、api生成  
纯vue版本演示地址：http://vue.whpj.vip  
混编vue thinkphp版本演示地址：http://vue2.whpj.vip  
混编vue webman版本演示地址：http://webman.whpj.vip  
混编vue hyperf版本演示地址：http://hyperf.whpj.vip  

## 功能介绍
封装有各种常用的表单组件，支持权限管理，支持阿里云、七牛云、腾讯云oss 上传，同时也支持客户端js上传到oss  直接在config/my.php 配置即可  

无需安装vue环境，无需编译、打包发布


### 运行环境
最低 php7.4  不支持php8.1
数据库配置 在 config/database.php 

### 安装说明
解析一个本地host域名 运行目录设置为public 注意配置好伪静态否则访问404  数据库文件在根目录data.sql  
账号:admin  密码:123456  
如果不会安装可以参考第一个视频  
http://www.vueadmin.net/html/shipinjiaocheng/admin/index.html


### 版权信息
请尊重作者的劳动成果，你可以免费学习并且使用商业项目，非授权版本请保留后台版权信息
禁止在vueadmin整体或任何部分基础上发展任何派生版本、修改版本或第三方版本用于重新分发。

# 免责
作者不保证也不能保证您由于使用本软件造成数据的丢失和由于在互联网上使用本软件带来的任何风险担保。作者不对任何情况下由本软件带来的损失承担责任，包括使用本软件、不能使用本软件和书面材料带来的利润损失，或其他附随性的损失和结果性损失。作者也不对由于黑客的进入而导致的数据库损失负责。



# 感谢（排名不分先后）
thinkphp  
element  



# 作者联系方式
作者qq：872977817  

![](https://www.workerman.net/upload/img/20220821/2163010bd2ba41.png)
![](https://www.workerman.net/upload/img/20220821/2163010bda0308.png)
![](https://www.workerman.net/upload/img/20220821/2163010be2c9c0.png)
![](https://www.workerman.net/upload/img/20220821/2163010beafdb6.png)
