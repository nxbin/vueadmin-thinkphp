Vue.component('Detail', {
	template: `
		<el-dialog title="查看详情" width="600px" class="icon-dialog" :visible.sync="show" @open="open" :before-close="closeForm" append-to-body>
			<table cellpadding="0" cellspacing="0" class="table table-bordered" align="center" width="100%" style="word-break:break-all; margin-bottom:30px;  font-size:13px;">
				<tbody>
					<tr>
						<td class="title" width="100">标题：</td>
						<td>
							{{form.title}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">性别：</td>
						<td>
							<span v-if="form.sex == '1'">男</span>
							<span v-if="form.sex == '2'">女</span>
						</td>
					</tr>
					<tr>
						<td class="title" width="100">状态：</td>
						<td>
							<span v-if="form.status == '1'">开启</span>
							<span v-if="form.status == '0'">关闭</span>
						</td>
					</tr>
					<tr>
						<td class="title" width="100">金额：</td>
						<td>
							{{form.amount}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">文本域：</td>
						<td>
							{{form.wb}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">标签：</td>
						<td>
							{{form.bq}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">排序号：</td>
						<td>
							{{form.sortid}}
						</td>
					</tr>
				</tbody>
			</table>
		</el-dialog>
	`
	,
	props: {
		show: {
			type: Boolean,
			default: true
		},
		size: {
			type: String,
			default: 'mini'
		},
		info: {
			type: Object,
		},
	},
	data() {
		return {
			form:{
			},
		}
	},
	methods: {
		open(){
			axios.post(base_url+'/Batch/detail',this.info).then(res => {
				this.form = res.data.data
			})
		},
		closeForm(){
			this.$emit('update:show', false)
		}
	}
})
