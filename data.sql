/*
 Navicat Premium Data Transfer

 Source Server         : vue
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : tp

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 21/08/2022 12:35:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cd_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `cd_admin_user`;
CREATE TABLE `cd_admin_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `user` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `pwd` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '所属分组',
  `note` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cd_admin_user
-- ----------------------------
INSERT INTO `cd_admin_user` VALUES (1, '寒塘冷月', 'admin', '7b2ad4e307d5b3ec9e786054225d0985', 1, '超级管理员', 1, 1548558919);
INSERT INTO `cd_admin_user` VALUES (32, '测试', 'test01', '7b2ad4e307d5b3ec9e786054225d0985', 52, '', 1, 1660878486);

-- ----------------------------
-- Table structure for cd_base_config
-- ----------------------------
DROP TABLE IF EXISTS `cd_base_config`;
CREATE TABLE `cd_base_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cd_base_config
-- ----------------------------
INSERT INTO `cd_base_config` VALUES (1, 'site_title', 'vueadmin后台管理系统');
INSERT INTO `cd_base_config` VALUES (2, 'logo', '/uploads/admin/202110/6162648e796a4.jpg');
INSERT INTO `cd_base_config` VALUES (3, 'keyword', 'vueadmin,快速生成a,不写一行代码');
INSERT INTO `cd_base_config` VALUES (4, 'descrip', '本系统全部基于vueadmin生成，不写一行代码快速开发');
INSERT INTO `cd_base_config` VALUES (5, 'copyright', '寒塘冷月 qq:872977817');
INSERT INTO `cd_base_config` VALUES (6, 'filesize', '100');
INSERT INTO `cd_base_config` VALUES (7, 'filetype', 'gif,png,jpg,jpeg,doc,docx,xls,xlsx,csv,pdf,rar,zip,txt,mp4,flv,wgt');
INSERT INTO `cd_base_config` VALUES (8, 'water_status', '0');
INSERT INTO `cd_base_config` VALUES (9, 'water_position', '5');
INSERT INTO `cd_base_config` VALUES (10, 'domain', '');
INSERT INTO `cd_base_config` VALUES (20, 'water_alpha', '90');

-- ----------------------------
-- Table structure for cd_batch
-- ----------------------------
DROP TABLE IF EXISTS `cd_batch`;
CREATE TABLE `cd_batch`  (
  `batch_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `sex` smallint(6) NULL DEFAULT NULL COMMENT '性别',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `sortid` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `wb` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文本域',
  `bq` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `jsq` decimal(10, 2) NULL DEFAULT NULL COMMENT '计数器',
  PRIMARY KEY (`batch_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_batch
-- ----------------------------
INSERT INTO `cd_batch` VALUES (1, '测试信息', 1, 1, 239.00, 1, '文本内容', 'layui', 4.00);
INSERT INTO `cd_batch` VALUES (8, '张三', 1, 1, 11.00, 2, '测试2', 'thinkphp', 3.00);
INSERT INTO `cd_batch` VALUES (10, '王五1', 1, 1, 31.00, 4, '寒塘冷月', 'xhadmin,xhcms', 1.00);
INSERT INTO `cd_batch` VALUES (12, '何影青', 2, 1, 100.00, 5, '文本域', 'vue', 5.00);
INSERT INTO `cd_batch` VALUES (13, '寒塘冷月', 1, 1, 568.00, 6, '会更好', 'xhadmin,vue', 4545.00);

-- ----------------------------
-- Table structure for cd_editor
-- ----------------------------
DROP TABLE IF EXISTS `cd_editor`;
CREATE TABLE `cd_editor`  (
  `editor_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `wangeditor` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'wangeditor编辑器',
  `tinymce` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'tinymce编辑器',
  `markdown` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'markdown编辑器',
  PRIMARY KEY (`editor_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_editor
-- ----------------------------
INSERT INTO `cd_editor` VALUES (1, '这是系统生成的编辑器组件', '<p>测试<img src=\"http://vue.whpj.vip/uploads/admin/202105/60af9043e29d0.jpg\" style=\"max-width: 100%;\"/></p>', '<p>在测试<img src=\"http://vue.whpj.vip/uploads/admin/202105/60af904d14069.jpg\" alt=\"\" width=\"200\" height=\"200\" /></p>', '![Deion](http://vue.whpj.vip/uploads/admin/202105/60af906eb84d6.jpg)');

-- ----------------------------
-- Table structure for cd_file
-- ----------------------------
DROP TABLE IF EXISTS `cd_file`;
CREATE TABLE `cd_file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filepath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件hash值',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `disk` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储类似',
  `type` tinyint(4) NULL DEFAULT NULL COMMENT '文件类型',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `hash`(`hash`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 327 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cd_file
-- ----------------------------
INSERT INTO `cd_file` VALUES (326, 'http://images.whpj.vip/admin/202208/cc608c637bec9117018cef0e4cc9c6dd.jpg', 'cc608c637bec9117018cef0e4cc9c6dd', 1660903432, 'qiniuyun', 1);

-- ----------------------------
-- Table structure for cd_goods
-- ----------------------------
DROP TABLE IF EXISTS `cd_goods`;
CREATE TABLE `cd_goods`  (
  `goods_id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `class_id` smallint(6) NULL DEFAULT NULL COMMENT '所属分类',
  `pic` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面图',
  `sale_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '销售价',
  `images` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图集',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `cd` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产地',
  `store` int(11) NULL DEFAULT NULL COMMENT '库存',
  `sortid` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '发布时间',
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容详情',
  `supplier_id` smallint(6) NULL DEFAULT NULL COMMENT '供应商',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_goods
-- ----------------------------
INSERT INTO `cd_goods` VALUES (1, '黑鲨游戏手机2 8GB+128GB 暗龙855 Magic', 12, 'http://vue.whpj.vip/uploads/admin/202105/60af8d752fe38.jpg', 210.00, '[{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d7d47a10.jpg\",\"name\":\"60af8d7d47a10.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d8de9d0f.jpg\",\"name\":\"60af8d8de9d0f.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af9043e29d0.jpg\",\"name\":\"60af9043e29d0.jpg\"}]', 1, '湖北', 999, 1, 1620044343, '<p>详细内容<img src=\"http://vue.whpj.vip/uploads/admin/202105/60af91ffbd6f8.jpg\" style=\"max-width: 100%;\"/></p>', 3, 1);
INSERT INTO `cd_goods` VALUES (2, '华为 HUAWEI 畅享10 极4800万超清夜景', 3, 'http://vue.whpj.vip/uploads/admin/202105/60af904d14069.jpg', 2345.00, '[{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d7d47a10.jpg\",\"name\":\"60af8d7d47a10.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d752fe38.jpg\",\"name\":\"60af8d752fe38.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af9043e29d0.jpg\",\"name\":\"60af9043e29d0.jpg\"}]', 1, '中国', 111, 2, 1620044702, '<p>测试刚刚好</p>', 2, NULL);
INSERT INTO `cd_goods` VALUES (3, 'ivo U1 3GB+32GB夜水滴全面屏 AI拍照手机	', 10, 'http://vue.whpj.vip/uploads/admin/202105/60af9200b555c.jpg', 4554.00, '[{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d7d47a10.jpg\",\"name\":\"60af8d7d47a10.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d8de9d0f.jpg\",\"name\":\"60af8d8de9d0f.jpg\"}]', 1, '武汉', 999, 100, 1620044929, '<p>测试跪求</p>', 1, 1);
INSERT INTO `cd_goods` VALUES (4, '这是三张表关联测试信息，列表方法多表配置查看如何配置的', 8, 'http://vue.whpj.vip/uploads/admin/202105/60af9200b555c.jpg', 2234.00, '[{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d85f101d.jpg\",\"name\":\"60af8d85f101d.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d7d47a10.jpg\",\"name\":\"60af8d7d47a10.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d8de9d0f.jpg\",\"name\":\"60af8d8de9d0f.jpg\"}]', 1, '', 0, 23, 1622120983, '<p>测试内哦让</p>', 1, 1);
INSERT INTO `cd_goods` VALUES (5, '九牧 JOMOO 不锈钢上水头编织软管 花洒喷头耐热防爆淋浴软管 H2BE2', 7, 'http://vue.whpj.vip/uploads/admin/202105/60af8d7d47a10.jpg', 445.00, '[{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af9200b555c.jpg\",\"name\":\"5ff40b8d7fbd2.jpg\"},{\"url\":\"\\/uploads\\/admin\\/202110\\/61627c797917c.png\",\"name\":\"5ff5dfbd5b60a.png\"}]', 1, '', 0, 5, 1633853113, '<p>fdfdfdfdfdfd</p>', 1, 1);
INSERT INTO `cd_goods` VALUES (6, '动态生成供应商联动商品分类效果，是不是很犀利', 12, 'http://vue.whpj.vip/uploads/admin/202105/60af94bfae6d2.jpg', 52.00, '[{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af9200b555c.jpg\",\"name\":\"60cf4aafa1322.jpg\"}]', 1, '湖北', 256, 6, 1652798364, '<p>测试</p>', 3, 1);

-- ----------------------------
-- Table structure for cd_goods_cata
-- ----------------------------
DROP TABLE IF EXISTS `cd_goods_cata`;
CREATE TABLE `cd_goods_cata`  (
  `class_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `pid` smallint(6) NULL DEFAULT NULL COMMENT '所属父类',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `sortid` int(11) NULL DEFAULT NULL COMMENT '排序',
  `supplier_id` smallint(6) NULL DEFAULT NULL COMMENT '供应商',
  PRIMARY KEY (`class_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_goods_cata
-- ----------------------------
INSERT INTO `cd_goods_cata` VALUES (1, '手机', NULL, 1, 1, 2);
INSERT INTO `cd_goods_cata` VALUES (2, '小米', 1, 1, 2, 2);
INSERT INTO `cd_goods_cata` VALUES (3, '华为', 1, 1, 3, 2);
INSERT INTO `cd_goods_cata` VALUES (4, '苹果', 1, 1, 4, 2);
INSERT INTO `cd_goods_cata` VALUES (5, '三星', 1, 1, 5, 2);
INSERT INTO `cd_goods_cata` VALUES (6, '电视', NULL, 1, 6, 1);
INSERT INTO `cd_goods_cata` VALUES (7, '小米', 6, 1, 7, 1);
INSERT INTO `cd_goods_cata` VALUES (8, 'htc', 6, 1, 8, 1);
INSERT INTO `cd_goods_cata` VALUES (9, 'LG', 6, 1, 9, 1);
INSERT INTO `cd_goods_cata` VALUES (10, '海信', 6, 1, 10, 1);
INSERT INTO `cd_goods_cata` VALUES (12, '蛋糕', NULL, 1, 12, 3);
INSERT INTO `cd_goods_cata` VALUES (13, '红米', 2, 1, 13, 2);
INSERT INTO `cd_goods_cata` VALUES (14, '苹果4s', 4, 1, 14, 2);
INSERT INTO `cd_goods_cata` VALUES (15, '小米电视', 7, 1, 15, 1);

-- ----------------------------
-- Table structure for cd_link
-- ----------------------------
DROP TABLE IF EXISTS `cd_link`;
CREATE TABLE `cd_link`  (
  `link_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接名称',
  `url` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接地址',
  `logo` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'logo',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `sortid` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `linkcata_id` smallint(6) NULL DEFAULT NULL COMMENT '所属分类',
  PRIMARY KEY (`link_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_link
-- ----------------------------
INSERT INTO `cd_link` VALUES (1, 'banner1', 'http://taobao.com', '/uploads/admin/202208/62ff4fa27db27.jpg', 1, 1, 1622121464, 4);
INSERT INTO `cd_link` VALUES (2, '搜狐', 'http://souhu.com', 'http://vue.whpj.vip/uploads/admin/202105/60af91ffbd6f8.jpg', 1, 2, 1622121586, 2);
INSERT INTO `cd_link` VALUES (3, '简书', 'http://jianshu.com', 'http://vue.whpj.vip/uploads/admin/202105/60af94abc6d3a.jpg', 1, 3, 1622121601, 2);
INSERT INTO `cd_link` VALUES (4, '网易', 'http://16311.com', 'http://vue.whpj.vip/uploads/admin/202105/60af8d7d47a10.jpg', 1, 4, 1622121615, 2);
INSERT INTO `cd_link` VALUES (5, '测试啊啊啊', 'http://baidu.com', 'http://vue.whpj.vip/uploads/admin/202105/60af8d7d47a10.jpg', 1, 1, 1632841268, 2);
INSERT INTO `cd_link` VALUES (6, 'banner2', 'http://baidu.com', '/uploads/admin/202208/62ff4fa27db27.jpg', 1, 6, 1636535153, 4);

-- ----------------------------
-- Table structure for cd_linkcata
-- ----------------------------
DROP TABLE IF EXISTS `cd_linkcata`;
CREATE TABLE `cd_linkcata`  (
  `linkcata_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `jdt` smallint(6) NULL DEFAULT NULL COMMENT '进度条',
  PRIMARY KEY (`linkcata_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_linkcata
-- ----------------------------
INSERT INTO `cd_linkcata` VALUES (2, '底部链接', 1, 41);
INSERT INTO `cd_linkcata` VALUES (3, '首页分类', 1, 18);
INSERT INTO `cd_linkcata` VALUES (4, '广告分类', 1, 48);
INSERT INTO `cd_linkcata` VALUES (5, '测试分类a', 1, 24);
INSERT INTO `cd_linkcata` VALUES (6, '默认分类', 1, 45);

-- ----------------------------
-- Table structure for cd_log
-- ----------------------------
DROP TABLE IF EXISTS `cd_log`;
CREATE TABLE `cd_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `application_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用名称',
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `url` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求url',
  `ip` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip',
  `useragent` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'useragent',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求内容',
  `errmsg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常信息',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `type` smallint(6) NULL DEFAULT NULL COMMENT '类型',
  `times` int(11) NULL DEFAULT NULL COMMENT '日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cd_map
-- ----------------------------
DROP TABLE IF EXISTS `cd_map`;
CREATE TABLE `cd_map`  (
  `map_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `bddt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '百度地图',
  `gddt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '高德地图',
  `txdt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '腾讯地图',
  PRIMARY KEY (`map_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_map
-- ----------------------------
INSERT INTO `cd_map` VALUES (1, '这是生成的地图组件', '{\"address\":\"武汉市武昌区梅园一路/梅园二路(路口)\",\"lng\":114.37015,\"lat\":30.540872}', '{\"address\":\"湖北省武汉市洪山区洪山街道保利中央公馆4期\",\"lng\":114.319124,\"lat\":30.494455}', '{\"address\":\"湖北省武汉市武昌区珞珈山16号武汉大学武汉大学-鉴湖\",\"lng\":114.363274,\"lat\":30.537601}');

-- ----------------------------
-- Table structure for cd_membe
-- ----------------------------
DROP TABLE IF EXISTS `cd_membe`;
CREATE TABLE `cd_membe`  (
  `membe_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `sex` smallint(6) NULL DEFAULT NULL COMMENT '性别',
  `pic` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `mobile` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `email` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `password` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '积分',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `ssq` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省市区',
  `tinymce` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '编辑器',
  `tinymce2` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '编辑器',
  PRIMARY KEY (`membe_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_membe
-- ----------------------------
INSERT INTO `cd_membe` VALUES (25, '韩梅梅', 1, 'http://vue.whpj.vip/uploads/admin/202105/60af8d8de9d0f.jpg', '13545028657', '274363574@qq.com', '978655f0ed9a6c3a33f104d64a2481a9', 22.00, 1, 1620211664, '山西省-大同市-城区', NULL, NULL);
INSERT INTO `cd_membe` VALUES (26, '李磊', 2, 'http://vue.whpj.vip/uploads/admin/202105/60af8d85f101d.jpg', '13545098761', '36732767@qq.com', '', 99.00, 1, 1620211664, '山东省-青岛市-市南区', NULL, NULL);
INSERT INTO `cd_membe` VALUES (27, '王五', 2, 'http://vue.whpj.vip/uploads/admin/202105/60af8d7d47a10.jpg', '13545098761', '36732767@qq.com', 'dfb32a9216356c14d12f162bfce8b35b', 102.00, 1, 1620211677, '山东省-青岛市-市南区', NULL, NULL);
INSERT INTO `cd_membe` VALUES (28, '李四a', 2, 'http://vue.whpj.vip/uploads/admin/202105/60af8d752fe38.jpg', '13545028657', '274363574@qq.com', 'f588c178d94d0bd418ba8ddfbff84184', 20.00, 1, 1620211677, '山西省-大同市-城区', NULL, NULL);
INSERT INTO `cd_membe` VALUES (29, '赵六', 1, 'http://vue.whpj.vip/uploads/admin/202105/60af9200b555c.jpg', '13545026847', '454512@qq.com', '6a5888d05ceb8033ebf0a3dfbf2b416e', 25.00, 1, 1633573323, '河北省-秦皇岛市-北戴河区', '<p>测试内容</p>', '<p>测试内容</p>');

-- ----------------------------
-- Table structure for cd_node
-- ----------------------------
DROP TABLE IF EXISTS `cd_node`;
CREATE TABLE `cd_node`  (
  `node_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `type` smallint(6) NULL DEFAULT NULL COMMENT '类型',
  `path` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点路径',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `icon` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `pid` smallint(6) NULL DEFAULT 0 COMMENT '所属父类',
  `sortid` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`node_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cd_node
-- ----------------------------
INSERT INTO `cd_node` VALUES (22, '首页', 1, '/admin/Index/main.html', 1, 'el-icon-s-home', 0, 1);
INSERT INTO `cd_node` VALUES (28, '会员管理', 1, '/admin/Membe/index.html', 1, 'el-icon-user-solid', 0, 2);
INSERT INTO `cd_node` VALUES (29, '表单输入框', 1, '/admin/Form', 1, 'el-icon-s-cooperation', 0, 5);
INSERT INTO `cd_node` VALUES (30, '编辑器', 1, '/admin/Editor/index.html', 1, '', 29, 30);
INSERT INTO `cd_node` VALUES (31, '地图组件', 1, '/admin/Map/index.html', 1, '', 29, 31);
INSERT INTO `cd_node` VALUES (32, '上传组件', 1, '/admin/Uploadfile/index.html', 1, '', 29, 32);
INSERT INTO `cd_node` VALUES (33, '其他组件', 1, '/admin/Other/index.html', 1, '', 29, 33);
INSERT INTO `cd_node` VALUES (41, '侧栏效果演示', 1, '/admin/Celan', 1, 'el-icon-link', 0, 6);
INSERT INTO `cd_node` VALUES (42, '友情链接分类', 1, '/admin/Linkcata/index.html', 1, '', 41, 42);
INSERT INTO `cd_node` VALUES (43, '友情链接', 1, '/admin/Link/index.html', 1, '', 41, 43);
INSERT INTO `cd_node` VALUES (44, '批量操作演示', 1, '/admin/Batch/index.html', 1, 'dripicons-wallet', 0, 44);
INSERT INTO `cd_node` VALUES (51, '配置管理', 1, '/admin/Config', 1, 'el-icon-s-data', 0, 51);
INSERT INTO `cd_node` VALUES (52, '基本配置', 1, '/admin/Baseconfig/index.html', 1, '', 51, 52);
INSERT INTO `cd_node` VALUES (54, '节点管理', 1, '/admin/Node/index.html', 1, '', 55, 59);
INSERT INTO `cd_node` VALUES (55, '系统管理', 1, '/admin/Sys', 1, 'el-icon-s-data', 0, 55);
INSERT INTO `cd_node` VALUES (56, '用户管理', 1, '/admin/Adminuser/index.html', 1, '', 55, 56);
INSERT INTO `cd_node` VALUES (57, '角色管理', 1, '/admin/Role/index.html', 1, '', 55, 57);
INSERT INTO `cd_node` VALUES (58, '日志管理', 1, '/admin/Log/index.html', 1, '', 55, 58);
INSERT INTO `cd_node` VALUES (59, '添加', 2, '/admin/Membe/add.html', 1, '', 28, 59);
INSERT INTO `cd_node` VALUES (60, '修改', 2, '/admin/Membe/update.html', 1, '', 28, 60);
INSERT INTO `cd_node` VALUES (61, '删除', 2, '/admin/Membe/delete.html', 1, '', 28, 61);
INSERT INTO `cd_node` VALUES (63, '抽屉打开方式', 1, '/admin/Mem/index', 1, '', NULL, 3);
INSERT INTO `cd_node` VALUES (64, '页面切换形式', 1, '/admin/Mem2/index', 1, '', 0, 4);
INSERT INTO `cd_node` VALUES (65, '商品管理', 1, '/admin/goods', 1, '', 0, 8);
INSERT INTO `cd_node` VALUES (66, '商品分类', 1, '/admin/Goodscata/index.html', 1, '', 65, 66);
INSERT INTO `cd_node` VALUES (67, '商品管理', 1, '/admin/Goods/index.html', 1, '', 65, 67);
INSERT INTO `cd_node` VALUES (68, '供应商管理', 1, '/admin/Supplier/index.html', 1, '', 65, 68);
INSERT INTO `cd_node` VALUES (69, '获取修改信息', 2, '/admin/Membe/getUpdateInfo.html', 1, '', 28, 69);

-- ----------------------------
-- Table structure for cd_other
-- ----------------------------
DROP TABLE IF EXISTS `cd_other`;
CREATE TABLE `cd_other`  (
  `other_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `jsq` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计数器',
  `tags` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `hk` smallint(6) NULL DEFAULT NULL COMMENT '滑块',
  `color` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色选择器',
  `jzd` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '键值对',
  `ssq` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省市区联动',
  `rate` smallint(6) NULL DEFAULT NULL COMMENT '评分',
  PRIMARY KEY (`other_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_other
-- ----------------------------
INSERT INTO `cd_other` VALUES (1, '生成的常用组件测试', '11', 'xhadmin,快速开发', 14, '#141313', '[{\"key\":\"A\",\"val\":\"中国\"},{\"key\":\"B\",\"val\":\"法国\"}]', '河北省-石家庄市-长安区', 4);
INSERT INTO `cd_other` VALUES (2, '测试西悉尼', '11', 'xhadmin,xhcms,vue', 28, '#EB0707', '[{\"key\":\"A\",\"val\":\"中国\"},{\"key\":\"B\",\"val\":\"发过\"},{\"key\":\"C\",\"val\":\"德国\"}]', '天津市-市辖区-和平区', 5);
INSERT INTO `cd_other` VALUES (4, '测试', '11', 'vue,git', 11, '#744242', '[{\"key\":\"A\",\"val\":\"测试\"}]', '天津市-市辖区-河西区', 1);
INSERT INTO `cd_other` VALUES (5, '地方的', '100', 'heyingm ', 44, '#4F92BC', '[{\"key\":\"A\",\"val\":\"中国\"},{\"key\":\"B\",\"val\":\"美国\"},{\"key\":\"C\",\"val\":\"德国\"}]', '山西省-阳泉市-郊区', 3);

-- ----------------------------
-- Table structure for cd_role
-- ----------------------------
DROP TABLE IF EXISTS `cd_role`;
CREATE TABLE `cd_role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组名称',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `pid` smallint(6) NULL DEFAULT NULL COMMENT '所属父类',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `access` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '权限节点',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 53 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cd_role
-- ----------------------------
INSERT INTO `cd_role` VALUES (1, '超级管理员', 1, 0, '超级管理员', '');
INSERT INTO `cd_role` VALUES (52, '普通管理员', 1, NULL, '', '/admin/Index/main.html,/admin/Membe/index.html,/admin/Membe/add.html,/admin/Membe/update.html,/admin/Membe/delete.html,/admin/Membe/getUpdateInfo.html,/admin/Link/index.html,Home');

-- ----------------------------
-- Table structure for cd_secrect
-- ----------------------------
DROP TABLE IF EXISTS `cd_secrect`;
CREATE TABLE `cd_secrect`  (
  `secrect_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `appid` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'appid',
  PRIMARY KEY (`secrect_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cd_secrect
-- ----------------------------
INSERT INTO `cd_secrect` VALUES (1, 'appid', 'HbUX4SClpbJv1', NULL);
INSERT INTO `cd_secrect` VALUES (2, 'secrect', 'OnLQ00vgWXzqohNc7Y2y', NULL);

-- ----------------------------
-- Table structure for cd_supplier
-- ----------------------------
DROP TABLE IF EXISTS `cd_supplier`;
CREATE TABLE `cd_supplier`  (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商名称',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`supplier_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_supplier
-- ----------------------------
INSERT INTO `cd_supplier` VALUES (1, '仟吉', 1, 1622120604, 'qianji', '7b2ad4e307d5b3ec9e786054225d0985');
INSERT INTO `cd_supplier` VALUES (2, '周黑鸭', 1, 1622120611, 'zhouheiya', '7b2ad4e307d5b3ec9e786054225d0985');
INSERT INTO `cd_supplier` VALUES (3, '皇冠蛋糕', 1, 1622120618, 'huangguan', '7b2ad4e307d5b3ec9e786054225d0985');
INSERT INTO `cd_supplier` VALUES (4, '精武鸭脖', 1, 1622120630, 'jingwu', '7b2ad4e307d5b3ec9e786054225d0985');

-- ----------------------------
-- Table structure for cd_uploadfile
-- ----------------------------
DROP TABLE IF EXISTS `cd_uploadfile`;
CREATE TABLE `cd_uploadfile`  (
  `uploadfile_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `pic` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单图上传',
  `pic_2` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单图2',
  `pics` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '多图上传',
  `file` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单文件',
  `files` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '多文件',
  PRIMARY KEY (`uploadfile_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cd_uploadfile
-- ----------------------------
INSERT INTO `cd_uploadfile` VALUES (1, '这是系统生成的上传组件测试', 'http://vue.whpj.vip/uploads/admin/202105/60af91ed8763f.jpg', 'http://vue.whpj.vip/uploads/admin/202105/60af9200b555c.jpg', '[{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d8de9d0f.jpg\",\"name\":\"60af8d8de9d0f.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d752fe38.jpg\",\"name\":\"60af8d752fe38.jpg\"},{\"url\":\"http:\\/\\/vue.whpj.vip\\/uploads\\/admin\\/202105\\/60af8d85f101d.jpg\",\"name\":\"60af8d85f101d.jpg\"}]', '/uploads/admin/202104/607e911312e48.jpg', '[{\"url\":\"http:\\/\\/zhangling.me\\/uploads\\/admin\\/202104\\/607e91189159c.jpg\",\"name\":\"607e910dc5e7b.jpg\"},{\"url\":\"http:\\/\\/zhangling.me\\/uploads\\/admin\\/202104\\/607e911896d47.jpg\",\"name\":\"607e9106a0212.jpg\"}]');

SET FOREIGN_KEY_CHECKS = 1;
