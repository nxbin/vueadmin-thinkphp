<?php 
/*
 module:		再加一种弹窗控制器
 create_time:	2022-08-19 16:40:44
 author:		
 contact:		
*/

namespace app\admin\controller;
use think\exception\ValidateException;
use app\admin\model\Mem2 as Mem2Model;
use think\facade\Db;

class Mem2 extends Admin {


	/*
 	* @Description  数据列表
 	*/
	function index(){
		if (!$this->request->isPost()){
			return view('index');
		}else{
			$limit  = $this->request->post('limit', 20, 'intval');
			$page = $this->request->post('page', 1, 'intval');

			$where = [];
			$where['membe_id'] = $this->request->post('membe_id', '', 'serach_in');
			$where['username'] = $this->request->post('username', '', 'serach_in');
			$where['sex'] = $this->request->post('sex', '', 'serach_in');
			$where['mobile'] = $this->request->post('mobile', '', 'serach_in');
			$where['email'] = $this->request->post('email', '', 'serach_in');
			$where['status'] = $this->request->post('status', '', 'serach_in');
			$where['ssq'] = ['like',implode('-',$this->request->post('ssq', [], 'serach_in'))];

			$create_time = $this->request->post('create_time', '', 'serach_in');
			$where['create_time'] = ['between',[strtotime($create_time[0]),strtotime($create_time[1])]];

			$field = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';

			$order  = $this->request->post('order', '', 'serach_in');	//排序字段
			$sort  = $this->request->post('sort', '', 'serach_in');		//排序方式

			$orderby = ($sort && $order) ? $sort.' '.$order : 'membe_id desc';

			$res = Mem2Model::where(formatWhere($where))->field($field)->order($orderby)->paginate(['list_rows'=>$limit,'page'=>$page])->toArray();

			$data['status'] = 200;
			$data['data'] = $res;
			return json($data);
		}
	}


	/*
 	* @Description  添加
 	*/
	public function add(){
		$postField = 'username,sex,pic,mobile,email,password,amount,status,ssq,create_time';
		$data = $this->request->only(explode(',',$postField),'post',null);

		$this->validate($data,\app\admin\validate\Mem2::class);

		$data['password'] = md5($data['password'].config('my.password_secrect'));
		$data['ssq'] = implode('-',$data['ssq']);
		$data['create_time'] = time();

		try{
			$res = Mem2Model::insertGetId($data);
		}catch(\Exception $e){
			throw new ValidateException($e->getMessage());
		}
		return json(['status'=>200,'data'=>$res,'msg'=>'添加成功']);
	}


	/*
 	* @Description  修改
 	*/
	public function update(){
		$postField = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';
		$data = $this->request->only(explode(',',$postField),'post',null);

		$this->validate($data,\app\admin\validate\Mem2::class);

		$data['ssq'] = implode('-',$data['ssq']);
		$data['create_time'] = strtotime($data['create_time']);

		try{
			Mem2Model::update($data);
		}catch(\Exception $e){
			throw new ValidateException($e->getMessage());
		}
		return json(['status'=>200,'msg'=>'修改成功']);
	}


	/*
 	* @Description  修改信息之前查询信息的 勿要删除
 	*/
	function getUpdateInfo(){
		$id =  $this->request->post('membe_id', '', 'serach_in');
		if(!$id) throw new ValidateException ('参数错误');
		$field = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';
		$res = Mem2Model::field($field)->find($id);
		$res['ssq'] = explode('-',$res['ssq']);
		return json(['status'=>200,'data'=>$res]);
	}


	/*
 	* @Description  删除
 	*/
	function delete(){
		$idx =  $this->request->post('membe_id', '', 'serach_in');
		if(!$idx) throw new ValidateException ('参数错误');
		Mem2Model::destroy(['membe_id'=>explode(',',$idx)],true);
		return json(['status'=>200,'msg'=>'操作成功']);
	}


	/*
 	* @Description  查看详情
 	*/
	function detail(){
		$id =  $this->request->post('membe_id', '', 'serach_in');
		if(!$id) throw new ValidateException ('参数错误');
		$field = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';
		$res = Mem2Model::field($field)->find($id);
		return json(['status'=>200,'data'=>$res]);
	}




}

