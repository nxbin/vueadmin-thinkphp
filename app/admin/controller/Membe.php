<?php 
/*
 module:		会员管理控制器
 create_time:	2022-08-19 17:06:02
 author:		
 contact:		
*/

namespace app\admin\controller;
use think\exception\ValidateException;
use app\admin\model\Membe as MembeModel;
use think\facade\Db;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Membe extends Admin {


	/*
 	* @Description  数据列表
 	*/
	function index(){
		if (!$this->request->isPost()){
			return view('index');
		}else{
			$limit  = $this->request->post('limit', 20, 'intval');
			$page = $this->request->post('page', 1, 'intval');

			$where = [];
			$where['membe_id'] = $this->request->post('membe_id', '', 'serach_in');
			$where['username'] = $this->request->post('username', '', 'serach_in');
			$where['sex'] = $this->request->post('sex', '', 'serach_in');
			$where['mobile'] = $this->request->post('mobile', '', 'serach_in');
			$where['email'] = $this->request->post('email', '', 'serach_in');
			$where['status'] = $this->request->post('status', '', 'serach_in');
			$where['ssq'] = ['like',implode('-',$this->request->post('ssq', [], 'serach_in'))];

			$create_time = $this->request->post('create_time', '', 'serach_in');
			$where['create_time'] = ['between',[strtotime($create_time[0]),strtotime($create_time[1])]];

			$field = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';

			$order  = $this->request->post('order', '', 'serach_in');	//排序字段
			$sort  = $this->request->post('sort', '', 'serach_in');		//排序方式

			$orderby = ($sort && $order) ? $sort.' '.$order : 'membe_id desc';

			$res = MembeModel::where(formatWhere($where))->field($field)->order($orderby)->paginate(['list_rows'=>$limit,'page'=>$page])->toArray();

			$data['status'] = 200;
			$data['data'] = $res;
			$data['sum_amount'] = MembeModel::where(formatWhere($where))->sum('amount');
			return json($data);
		}
	}


	/*
 	* @Description  修改排序开关
 	*/
	function updateExt(){
		$postField = 'membe_id,status';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(!$data['membe_id']) throw new ValidateException ('参数错误');
		MembeModel::update($data);
		return json(['status'=>200,'msg'=>'操作成功']);
	}

	/*
 	* @Description  添加
 	*/
	public function add(){
		$postField = 'username,sex,pic,mobile,email,password,amount,status,ssq,create_time';
		$data = $this->request->only(explode(',',$postField),'post',null);

		$this->validate($data,\app\admin\validate\Membe::class);

		$data['password'] = md5($data['password'].config('my.password_secrect'));
		$data['ssq'] = implode('-',$data['ssq']);
		$data['create_time'] = time();

		try{
			$res = MembeModel::insertGetId($data);
		}catch(\Exception $e){
			throw new ValidateException($e->getMessage());
		}
		return json(['status'=>200,'data'=>$res,'msg'=>'添加成功']);
	}


	/*
 	* @Description  修改
 	*/
	public function update(){
		$postField = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';
		$data = $this->request->only(explode(',',$postField),'post',null);

		$this->validate($data,\app\admin\validate\Membe::class);

		$data['ssq'] = implode('-',$data['ssq']);
		$data['create_time'] = strtotime($data['create_time']);

		try{
			MembeModel::update($data);
		}catch(\Exception $e){
			throw new ValidateException($e->getMessage());
		}
		return json(['status'=>200,'msg'=>'修改成功']);
	}


	/*
 	* @Description  修改信息之前查询信息的 勿要删除
 	*/
	function getUpdateInfo(){
		$id =  $this->request->post('membe_id', '', 'serach_in');
		if(!$id) throw new ValidateException ('参数错误');
		$field = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';
		$res = MembeModel::field($field)->find($id);
		$res['ssq'] = explode('-',$res['ssq']);
		return json(['status'=>200,'data'=>$res]);
	}


	/*
 	* @Description  删除
 	*/
	function delete(){
		$idx =  $this->request->post('membe_id', '', 'serach_in');
		if(!$idx) throw new ValidateException ('参数错误');
		MembeModel::destroy(['membe_id'=>explode(',',$idx)],true);
		return json(['status'=>200,'msg'=>'操作成功']);
	}


	/*
 	* @Description  查看详情
 	*/
	function detail(){
		$id =  $this->request->post('membe_id', '', 'serach_in');
		if(!$id) throw new ValidateException ('参数错误');
		$field = 'membe_id,username,sex,pic,mobile,email,amount,status,ssq,create_time';
		$res = MembeModel::field($field)->find($id);
		return json(['status'=>200,'data'=>$res]);
	}


	/*
 	* @Description  重置密码
 	*/
	public function resetPwd(){
		$postField = 'membe_id,password';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(empty($data['membe_id'])) throw new ValidateException ('参数错误');
		if(empty($data['password'])) throw new ValidateException ('密码不能为空');

		$data['password'] = md5($data['password'].config('my.password_secrect'));
		$res = MembeModel::update($data);
		return json(['status'=>200,'msg'=>'操作成功']);
	}


	/*
 	* @Description  数值加
 	*/
	public function jia(){
		$postField = 'membe_id,amount';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(empty($data['membe_id'])) throw new ValidateException ('参数错误');
		if(empty($data['amount'])) throw new ValidateException ('值不能为空');
		$res = MembeModel::field('amount')->where('membe_id',$data['membe_id'])->inc('amount',$data['amount'])->update();
		return json(['status'=>200,'msg'=>'操作成功']);
	}


	/*
 	* @Description  数值减
 	*/
	public function jian(){
		$postField = 'membe_id,amount';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(empty($data['membe_id'])) throw new ValidateException ('参数错误');
		if(empty($data['amount'])) throw new ValidateException ('值不能为空');

		if($data['amount'] > MembeModel::where('membe_id',$data['membe_id'])->value('amount')){
			throw new ValidateException('数据不足');
		}
		$res = MembeModel::field('amount')->where('membe_id',$data['membe_id'])->dec('amount',$data['amount'])->update();
		return json(['status'=>200,'msg'=>'操作成功']);
	}


	/*
 	* @Description  导入
 	*/
	public function importData(){
		$data = $this->request->post();
		$list = [];
		foreach($data as $key=>$val){
			$list[$key]['username'] = $val['用户名'];
			$list[$key]['sex'] = getValByKey($val['性别'],'[{"key":"男","val":"1","label_color":"primary"},{"key":"女","val":"2","label_color":"warning"}]');
			$list[$key]['pic'] = $val['头像'];
			$list[$key]['mobile'] = $val['手机号'];
			$list[$key]['email'] = $val['邮箱'];
			$list[$key]['password'] = $val['密码'] ? md5($val['密码']) : '';
			$list[$key]['amount'] = $val['积分'];
			$list[$key]['status'] = getValByKey($val['状态'],'[{"key":"开启","val":"1"},{"key":"关闭","val":"0"}]');
			$list[$key]['ssq'] = $val['省市区'];
			$list[$key]['create_time'] = time();
		}
		(new MembeModel)->insertAll($list);
		return json(['status'=>200]);
	}


	/*
 	* @Description  客户端导出
 	*/
	function dumpdata(){
		$page = $this->request->param('page', 1, 'intval');
		$limit = config('my.dumpsize') ? config('my.dumpsize') : 1000;

		$state = $this->request->param('state');
		$where = [];
		$where['membe_id'] = ['in',$this->request->param('membe_id', '', 'serach_in')];
		$where['username'] = $this->request->param('username', '', 'serach_in');
		$where['sex'] = $this->request->param('sex', '', 'serach_in');
		$where['mobile'] = $this->request->param('mobile', '', 'serach_in');
		$where['email'] = $this->request->param('email', '', 'serach_in');
		$where['status'] = $this->request->param('status', '', 'serach_in');
		$where['ssq'] = ['like',implode('-',$this->request->param('ssq', [], 'serach_in'))];

		$create_time = $this->request->param('create_time', '', 'serach_in');
		$where['create_time'] = ['between',[strtotime($create_time[0]),strtotime($create_time[1])]];

		$order  = $this->request->param('order', '', 'serach_in');	//排序字段
		$sort  = $this->request->param('sort', '', 'serach_in');		//排序方式

		$orderby = ($sort && $order) ? $sort.' '.$order : 'membe_id desc';

		$field = 'username,sex,pic,mobile,email,password,amount,status,ssq,create_time';

		$res = MembeModel::where(formatWhere($where))->field($field)->order($orderby)->paginate(['list_rows'=>$limit,'page'=>$page])->toArray();

		foreach($res['data'] as $key=>$val){
			$res['data'][$key]['sex'] = getItemVal($val['sex'],'[{"key":"男","val":"1","label_color":"primary"},{"key":"女","val":"2","label_color":"warning"}]');
			$res['data'][$key]['status'] = getItemVal($val['status'],'[{"key":"开启","val":"1"},{"key":"关闭","val":"0"}]');
			$res['data'][$key]['create_time'] = !empty($val['create_time']) ? date('Y-m-d H:i:s',$val['create_time']) : '';
			unset($res['data'][$key]['membe_id']);
		}

		$data['status'] = 200;
		$data['header'] = explode(',','用户名,性别,头像,手机号,邮箱,密码,积分,状态,省市区,创建时间');
		$data['percentage'] = ceil($page * 100/ceil($res['total']/$limit));
		$data['filename'] = '会员管理.'.config('my.dump_extension');
		$data['data'] = $res['data'];
		return json($data);
	}


	/*
 	* @Description  服务端导出
 	*/
	function dumpdata2(){
		$page = $this->request->param('page', 1, 'intval');
		$limit = config('my.dumpsize') ? config('my.dumpsize') : 1000;

		$state = $this->request->param('state');
		$where = [];
		$where['membe_id'] = ['in',$this->request->param('membe_id', '', 'serach_in')];
		$where['username'] = $this->request->param('username', '', 'serach_in');
		$where['sex'] = $this->request->param('sex', '', 'serach_in');
		$where['mobile'] = $this->request->param('mobile', '', 'serach_in');
		$where['email'] = $this->request->param('email', '', 'serach_in');
		$where['status'] = $this->request->param('status', '', 'serach_in');
		$where['ssq'] = ['like',implode('-',$this->request->param('ssq', [], 'serach_in'))];

		$create_time = $this->request->param('create_time', '', 'serach_in');
		$where['create_time'] = ['between',[strtotime($create_time[0]),strtotime($create_time[1])]];

		$order  = $this->request->param('order', '', 'serach_in');	//排序字段
		$sort  = $this->request->param('sort', '', 'serach_in');		//排序方式

		$orderby = ($sort && $order) ? $sort.' '.$order : 'membe_id desc';

		$field = 'username,sex,pic,mobile,email,password,amount,status,ssq,create_time';

		$res = MembeModel::where(formatWhere($where))->field($field)->order($orderby)->paginate(['list_rows'=>$limit,'page'=>$page])->toArray();

		$cache_key = 'Membe';
		if($page == 1){
			cache($cache_key,null);
			cache($cache_key,[]);
		}
		if($res['data']){
			cache($cache_key,array_merge(cache($cache_key),$res['data']));
			$data['percentage'] = ceil($page * 100/ceil($res['total']/$limit));
			$data['state'] =  'ok';
			return json($data);
		}
		if($state == 'ok'){
			$spreadsheet = new Spreadsheet();
			$sheet = $spreadsheet->getActiveSheet();

			$sheet->setCellValue('A1','用户名');
			$sheet->setCellValue('B1','性别');
			$sheet->setCellValue('C1','头像');
			$sheet->setCellValue('D1','手机号');
			$sheet->setCellValue('E1','邮箱');
			$sheet->setCellValue('F1','密码');
			$sheet->setCellValue('G1','积分');
			$sheet->setCellValue('H1','状态');
			$sheet->setCellValue('I1','省市区');
			$sheet->setCellValue('J1','创建时间');

			foreach(cache($cache_key) as $k=>$v){
				$sheet->setCellValue('A'.($k+2),$v['username']);
				$sheet->setCellValue('B'.($k+2),getItemVal($v['sex'],'[{"key":"男","val":"1","label_color":"primary"},{"key":"女","val":"2","label_color":"warning"}]'));
				$sheet->setCellValue('C'.($k+2),$v['pic']);
				$sheet->setCellValue('D'.($k+2),$v['mobile']);
				$sheet->setCellValue('E'.($k+2),$v['email']);
				$sheet->setCellValue('F'.($k+2),$v['password']);
				$sheet->setCellValue('G'.($k+2),$v['amount']);
				$sheet->setCellValue('H'.($k+2),getItemVal($v['status'],'[{"key":"开启","val":"1"},{"key":"关闭","val":"0"}]'));
				$sheet->setCellValue('I'.($k+2),$v['ssq']);
				$sheet->setCellValue('J'.($k+2),!empty($v['create_time']) ? date('Y-m-d H:i:s',$v['create_time']) : '');
			}

			$filename = '会员管理.'.config('my.dump_extension');
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename='.$filename);
 			header('Cache-Control: max-age=0');
			$writer = new Xlsx($spreadsheet);
			$writer->save('php://output');exit;
		}
	}


	/*
 	* @Description  禁用
 	*/
	public function forbidden(){
		$idx = $this->request->post('membe_id', '', 'serach_in');
		if(empty($idx)) throw new ValidateException ('参数错误');

		$data['status'] = 0;
		$res = MembeModel::where(['membe_id'=>explode(',',$idx)])->update($data);
		return json(['status'=>200,'msg'=>'操作成功']);
	}


	/*
 	* @Description  启用
 	*/
	public function start(){
		$idx = $this->request->post('membe_id', '', 'serach_in');
		if(empty($idx)) throw new ValidateException ('参数错误');

		$data['status'] = 1;
		$res = MembeModel::where(['membe_id'=>explode(',',$idx)])->update($data);
		return json(['status'=>200,'msg'=>'操作成功']);
	}




}

